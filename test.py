# encoding: utf-8
# -*- mode: python -*-

import os
import unittest

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from yawn import get_driver


class TestCase(unittest.TestCase):

    def setUp(self):
        browser = os.environ.get('BROWSER')
        if not browser:
            raise ValueError('No BROWSER environment variable set')
        seleniumip = os.environ.get('SELENIUMIP')
        self.driver = get_driver(browser, seleniumip)
        self.driver.set_window_size(1024, 768)
        self.browser = browser
        self.seleniumip = seleniumip

    def tearDown(self):
        self.driver.quit()

    def testPageTitle(self):
        self.driver.get("http://jimmyg.org/")
        content = self.driver.find_element_by_id('content')
        assert "James Gardner" in content.get_attribute("innerHTML")
        self.driver.save_screenshot('home1.png')
        self.driver.execute_script("""
            document.body.innerHTML = '<div id="newcontent">Selenium</div>';
        """)
        WebDriverWait(self.driver, 2).until(
            expected_conditions.presence_of_element_located(
                (By.ID, "newcontent")
            )
        )
        self.driver.save_screenshot('home2.png')
        # IE8 doesn't have a console object, so we can't run this test
        if self.browser.lower() != 'ie8':
            self.driver.execute_script('console.error("Hello")')
        if self.browser.lower() in ['chrome', 'safari']:
            self.assertIn(
                u'Hello',
                self.driver.get_log('browser')[-1][u'message']
            )


if __name__ == '__main__':
    unittest.main(verbosity=2)
