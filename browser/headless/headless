#!/bin/sh

set -e


PACKAGES="libexif12 openjdk-7-jre google-chrome-stable xvfb unzip firefox imagemagick"


on_exit () {
    if [ $? -eq 0 ]
    then
        if [ -e /etc/init.d/headless ]; then
            sudo /etc/init.d/headless start
        fi
        echo "Success!"
    else
        echo "Failed."
    fi
}
trap on_exit EXIT


ensure_chrome_signing_key () {
    if ! sudo apt-key list | grep "Google, Inc. Linux Package Signing Key <linux-packages-keymaster@google.com>" > /dev/null; then
        echo "Adding the Google Chrome signing key and apt repo ..."
        wget -q -O - "https://dl-ssl.google.com/linux/linux_signing_key.pub" | sudo apt-key add -
        echo "done"
    fi
}


ensure_chrome_apt_repo () {
    if [ ! -e /etc/apt/sources.list.d/google-chrome.list ]; then
        echo "Adding the Google Chrome signing key and apt repo ..."
        echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list
        echo "done."
    fi
}


package_installed () {
    dpkg -s "$1" >/dev/null 2>&1
}


ensure_chrome_driver () {
    # Should we always compare the versions and upgrade
    if [ ! -e /usr/bin/chromedriver ]; then
        cur_dir=$PWD
        tmp_dir=$( mktemp -d /tmp/testbed_install.XXXX )
        echo "Fetching and installing chromedriver (using ${tmp_dir}) ..."
        cd "$tmp_dir"
        chrome_driver_version=$( wget -qO- http://chromedriver.storage.googleapis.com/LATEST_RELEASE )
        echo "Latest version is $chrome_driver_version"
        wget -q "http://chromedriver.storage.googleapis.com/${chrome_driver_version}/chromedriver_linux64.zip"
        unzip chromedriver_linux64.zip
        sudo mv chromedriver /usr/bin
        sudo chmod a+x /usr/bin/chromedriver
        cd "${cur_dir}"
        rm -r "$tmp_dir"
        echo "done."
    fi
}


ensure_selenium () {
    if [ ! -e /usr/local/bin/selenium-server-standalone.jar ]; then
        echo "Fetching and installing selenium-server-standalone.jar ... "
        sudo wget -O /usr/local/bin/selenium-server-standalone.jar -q "http://selenium-release.storage.googleapis.com/2.43/selenium-server-standalone-2.43.1.jar"
        echo "done."
    fi
}


install () {
    ensure_chrome_signing_key
    ensure_chrome_apt_repo
    to_install=""
    for package in $PACKAGES ; do
        if ! package_installed "$package" ; then
            to_install="${to_install} ${package}"
        fi
    done
    if [ ! "x${to_install}" = "x" ]; then
        echo "Updating sources ..."
        if ! sudo apt-get update > /dev/null; then
            echo "*********** Failed, but continuing anyway ************"
        fi
        echo "Installing ${to_install} ..."
        sudo apt-get install -y ${to_install} > /dev/null
        echo "done."
    fi
    ensure_chrome_driver
    ensure_selenium
    ensure_init_script
}


upgrade () {
    echo "Stopping headless ..."
    if [ -e /etc/init.d/headless ]; then
        sudo /etc/init.d/headless stop > /dev/null
    fi
    echo "done."
    echo "Updating sources ..."
    if ! sudo apt-get update > /dev/null; then
        echo "*********** Failed, but continuing anyway ************"
    fi
    echo "done."
    echo "Upgrading ..."
    sudo apt-get upgrade > /dev/null
    echo "done."
    if [ -e /usr/bin/chromedriver ]; then
        sudo rm /usr/bin/chromedriver
    fi
    if [ -e /usr/local/bin/selenium-server-standalone.jar ]; then
        sudo rm /usr/local/bin/selenium-server-standalone.jar
    fi
    sudo update-rc.d -f headless remove > /dev/null
    if [ -e /etc/init.d/headless ]; then
        sudo rm /etc/init.d/headless
    fi
    ensure_chrome_signing_key
    ensure_chrome_apt_repo
    to_install=""
    for package in $PACKAGES ; do
        if ! package_installed $package ; then
            to_install="$to_install $package"
        fi
    done
    if [ ! "x$to_install" = "x" ]; then
        echo "Installing $to_install ..."
        sudo apt-get install -y $to_install > /dev/null
        echo "done."
    fi
    ensure_chrome_driver
    ensure_selenium
    ensure_init_script
}


ensure_init_script () {
    if [ ! -e /etc/init.d/headless ]; then
        echo "Adding an init script ..."
        cat << "EOF" | sudo tee /etc/init.d/headless > /dev/null
#!/bin/bash

NAME=headless
USER=nobody
COMMAND=
export DISPLAY=:10

### BEGIN INIT INFO
# Provides:          $NAME
# Required-Start:    $remote_fs $syslog $network
# Required-Stop:     $remote_fs $syslog $network
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Starts a headless chrome
# Description:       Starts chrome as a headless daemon at boot time
### END INIT INFO

start () {
    echo "Starting Xvfb ..."
    /sbin/start-stop-daemon \
        --start \
        --user $USER --group $USER --chuid $USER \
        --background --make-pidfile --pidfile /var/run/${NAME}-xvfb.pid \
        --exec /usr/bin/Xvfb -- $DISPLAY -screen 0 1366x768x24 -ac
    echo "done."
    echo "Starting Google Chrome ..."
    /sbin/start-stop-daemon \
        --start \
        --user $USER --group $USER --chuid $USER \
        --background --make-pidfile --pidfile /var/run/${NAME}-chrome.pid \
        --exec /usr/bin/google-chrome -- --remote-debugging-port=9222
    echo "done."
    echo "Starting Selenium ..."
        DISPLAY=:10 /usr/bin/java -jar /usr/local/bin/selenium-server-standalone.jar &> /var/log/selenium.log &
        java_pid=$!
        echo "Java pid is $java_pid"
        echo $java_pid > "/var/run/${NAME}-selenium.pid"
    echo "done."
    sleep 1
}


stop () {
    if [ -e "/var/run/${NAME}-selenium.pid" ]; then
        echo "Stopping selenium"
        killall java # $( cat "/var/run/${NAME}-selenium.pid" )
        rm "/var/run/${NAME}-selenium.pid"
        echo "done."
    fi
    if [ -e "/var/run/${NAME}-chrome.pid" ]; then
        echo "Stopping chrome"
        pkill -P $( cat "/var/run/${NAME}-chrome.pid" )
        rm "/var/run/${NAME}-chrome.pid"
        echo "done."
    fi
    if [ -e "/var/run/${NAME}-xvfb.pid" ]; then
        echo "Stopping Xvfb"
        pkill -P $( cat "/var/run/${NAME}-xvfb.pid" )
        rm "/var/run/${NAME}-xvfb.pid"
        echo "done."
    fi
    if ps aux | grep firefox | grep -v grep > /dev/null; then
        echo "Stopping Firefox"
        killall firefox
        echo "done."
    fi
    echo "Stopped."
}


case "$1" in
  restart)
    stop
    start
    ;;
  start)
    start
    ;;
  stop)
    stop
    ;;
  *)
    echo "Usage: /etc/init.d/headless {start|stop|restart}"
    exit 1
    ;;
esac
EOF
        sudo chmod +x /etc/init.d/headless
        sudo update-rc.d -f headless defaults > /dev/null
        echo "done."
    fi
}

case "$1" in
  install)
    install
    ;;
  upgrade)
    upgrade
    ;;
  *)
    echo "Usage: bootstrap {install|upgrade}"
    exit 1
    ;;
esac

